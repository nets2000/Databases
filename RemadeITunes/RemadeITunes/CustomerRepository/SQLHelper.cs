﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using RemadeITunes.Customers;

namespace RemadeITunes.CustomerRepository
{
    public class SQLHelper : ICustomerRepository
    {
        public static SQLHelper instance = null;

        private SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

        public int AmountOfCustomers { get; set; } = 0;

        /// <summary>
        /// Create a singleton of the sql helper object so it is accessible everywhere
        /// Create the connection string
        /// Get the amount of customers in the database
        /// </summary>
        public SQLHelper()
        {
            if (instance == null)
            {
                instance = this;

                CreateConnectionString();
                AmountOfCustomers = GetAmountOfCustomers();
            }
        }

        /// <summary>
        /// Create the connection string for connecting correctly to database
        /// </summary>
        public void CreateConnectionString()
        {
            builder.DataSource = @"MSI\SQLEXPRESS01";
            builder.InitialCatalog = "Chinook";
            builder.IntegratedSecurity = true;
            builder.Encrypt = false;
        }

        /// <summary>
        /// Counts the amount of customers in the database
        /// </summary>
        /// <returns>The amount of customers in database</returns>
        public int GetAmountOfCustomers()
        {
            string sql = GetCustomerDataQuery();
            int counter = 0;
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        try
                        {
                            using(SqlDataReader reader = command.ExecuteReader())
                            {
                                while(reader.Read())
                                {
                                    counter++;
                                }
                            }
                        }
                        catch (SqlException error)
                        {
                            Console.WriteLine("Sql error: " + error.Message);
                        }
                    }
                }
            }
            catch (Exception error)
            {
                Console.WriteLine("Sql connection error: " + error.Message);
            }

            return counter;
        }

        /// <summary>
        /// Creates a list with personal data of all the customers in the database
        /// </summary>
        /// <returns>A list of customers with their personal data</returns>
        public List<Customer> GetAllCustomers()
        {
            return PerformSqlQuery(GetCustomerDataQuery(), "All customers in database:");
        }

        /// <summary>
        /// Reads the personal data of a single customer selected by the ID
        /// </summary>
        /// <param name="id">ID of the requested customer</param>
        /// <returns>A single customer with their personal data</returns>
        public Customer GetCustomerByID(int id)
        {
            return PerformSqlQuery($"{GetCustomerDataQuery()} WHERE CustomerId = {id}", 
                "Customer with given id:")[0];
        }

        /// <summary>
        /// Reads the personal data of customers selected by (part of) the name
        /// </summary>
        /// <param name="name">(part of) Name of the requested customers</param>
        /// <returns>A list of customers with their personal data</returns>
        public List<Customer> GetCustomersByName(string name)
        {
            return PerformSqlQuery($"{GetCustomerDataQuery()} WHERE FirstName LIKE '%{name}%'", "All customers with name:");
        }

        /// <summary>
        /// Creates a page with a list of customers depending on the start index and amount of customers per page
        /// </summary>
        /// <param name="start">the index of the first customer to display</param>
        /// <param name="amount">the amount of customers to display on the page</param>
        /// <returns>A list of customers with their personal data</returns>
        public List<Customer> GetPageOfCustomers(int start, int amount)
        {
            return PerformSqlQuery($"{GetCustomerDataQuery()} ORDER BY CustomerId OFFSET {start - 1} ROWS FETCH NEXT {amount} ROWS ONLY", 
                "All customers in selected page:");
        }

        /// <summary>
        /// Adds a new customer to the database
        /// </summary>
        /// <param name="customer">Customer that will be added to the database</param>
        /// <returns>A bool that is true if a customer is succesfully added to the database, else returns false</returns>
        public bool AddNewCustomer(Customer customer)
        {
            string sql = $"INSERT INTO Customer ({GetCustomerBaseDataColumns()}) VALUES " +
                $"(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    try
                    {
                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                            command.Parameters.AddWithValue("@LastName", customer.LastName);
                            command.Parameters.AddWithValue("@Country", customer.Country);
                            command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                            command.Parameters.AddWithValue("@Phone", customer.Phone);
                            command.Parameters.AddWithValue("@Email", customer.Email);

                            command.ExecuteNonQuery();
                        }
                    } catch (SqlException error)
                    {
                        Console.WriteLine("Sql error: " + error.Message);
                        return false;
                    }
                }
            } catch (Exception error)
            {
                Console.WriteLine("Connection error: " + error.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Updates a customer selected by ID from user input
        /// </summary>
        /// <param name="id">The ID of the customer that needs to be updated</param>
        /// <param name="customer">The new customer with updated values</param>
        /// <returns>A bool that is true if a customer is succesfully updated in the database, else returns false</returns>
        public bool UpdateCustomer(int id, Customer customer)
        {
            string sql = $"UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, " +
                $"PostalCode = @PostalCode, Phone = @Phone, Email = @Email WHERE CustomerId = {id}";

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    try
                    {
                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                            command.Parameters.AddWithValue("@LastName", customer.LastName);
                            command.Parameters.AddWithValue("@Country", customer.Country);
                            command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                            command.Parameters.AddWithValue("@Phone", customer.Phone);
                            command.Parameters.AddWithValue("@Email", customer.Email);

                            command.ExecuteNonQuery();
                        }
                    }
                    catch (SqlException error)
                    {
                        Console.WriteLine("Sql error: " + error.Message);
                        return false;
                    }
                }
            }
            catch (Exception error)
            {
                Console.WriteLine("Connection error: " + error.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Calculates the amount of customers per country and puts them in a descending order
        /// </summary>
        /// <returns>A dictionary where string is the country and int is the amount of customers in that country</returns>
        public Dictionary<string, int> GetAmountOfCustomersPerCountry()
        {
            List<Customer> customers = PerformSqlQuery(GetCustomerDataQuery(), "Amount of customers per country:");
            Dictionary<string, int> customersPerCountry = new Dictionary<string, int>();

            for (int i = 0; i < customers.Count; i++)
            {
                if (!customersPerCountry.ContainsKey(customers[i].Country))
                {
                    customersPerCountry.Add(customers[i].Country, 1);
                } else
                {
                    customersPerCountry[customers[i].Country]++;
                }
            }
            return customersPerCountry;
        }

        /// <summary>
        /// Calculates the money spended per customer and puts them in a descending order
        /// </summary>
        /// <returns>A dictionary where string is the name of the customer and float is the amount of money that customer spend</returns>
        public Dictionary<string, float> GetAmountOfMoneySpendPerCustomer()
        {
            Dictionary<string, float> moneySpendPerCustomer = new Dictionary<string, float>();
            string sql = "SELECT Customer.FirstName, Customer.LastName, Invoice.Total FROM Customer " +
                "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId";

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    try
                    {
                        connection.Open();

                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    string name = $"{reader.GetString(0)} {reader.GetString(1)}";
                                    float total = (float)reader.GetDecimal(2);

                                    if (!moneySpendPerCustomer.ContainsKey(name))
                                    {
                                        moneySpendPerCustomer.Add(name, total);
                                    } else
                                    {
                                        moneySpendPerCustomer[name] += total;
                                    }
                                }
                            }
                        }
                    }
                    catch (SqlException error)
                    {
                        Console.WriteLine("Sql error: " + error.Message);
                    }
                }
            }
            catch (Exception error)
            {
                Console.WriteLine("Sql connection error: " + error.Message);
            }

            return moneySpendPerCustomer;
        }

        /// <summary>
        /// Calculates the amount of bought tracks per genre from a  single customer
        /// </summary>
        /// <param name="id">The ID of the customer to request the favourite genre</param>
        /// <returns>A customer genre that holds the name and favorite genres</returns>
        public CustomerGenre GetFavouriteGenresFromCustomer(int id)
        {
            Dictionary<string, int> amountPerGenre = new Dictionary<string, int>();
            string favoriteGenres = "";
            string firstName = "";
            string lastName = "";

            string sql = "SELECT Genre.Name, Customer.FirstName, Customer.LastName FROM (((Genre " +
                "INNER JOIN Track ON Genre.GenreId = Track.GenreId) " +
                "INNER JOIN InvoiceLine ON InvoiceLine.TrackId = Track.TrackId) " +
                "INNER JOIN Invoice AS InvoiceToInvoiceLine ON InvoiceToInvoiceLine.InvoiceId = InvoiceLine.InvoiceId), (Customer " +
                "INNER JOIN Invoice AS InvoiceToCustomer ON Customer.CustomerId = InvoiceToCustomer.CustomerId) " +
                $"WHERE InvoiceToInvoiceLine.CustomerId = {id} AND InvoiceToCustomer.CustomerId = {id}";

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    try
                    {
                        connection.Open();

                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    firstName = reader.GetString(1);
                                    lastName = reader.GetString(2);

                                    string genre = reader.GetString(0);
                                    if (!amountPerGenre.ContainsKey(genre))
                                    {
                                        amountPerGenre.Add(genre, 1);
                                    }
                                    else
                                    {
                                        amountPerGenre[genre]++;
                                    }
                                }
                            }
                        }
                    }
                    catch (SqlException error)
                    {
                        Console.WriteLine("Sql error: " + error.Message);
                    }
                }
            }
            catch (Exception error)
            {
                Console.WriteLine("Sql connection error: " + error.Message);
            }

            int highestValue = -1;
            foreach (KeyValuePair<string, int> entry in amountPerGenre)
            {
                if (entry.Value > highestValue)
                {
                    highestValue = entry.Value;
                }
            }

            var highestGenres = amountPerGenre.Where(pair => pair.Value == highestValue).Select(pair => pair.Key);
            foreach (string genre in highestGenres)
            {
                favoriteGenres += genre + "\n";
            }
            favoriteGenres.Trim();

            return new CustomerGenre { FirstName = firstName, LastName = lastName, FavoriteGenres = favoriteGenres };
        }

        /// <summary>
        /// Helper method to create the SQL string for requesting the customers personal data from the database
        /// </summary>
        /// <returns>A SQL string to use for requesting personal data</returns>
        public string GetCustomerDataQuery()
        {
            return $"SELECT CustomerId, {GetCustomerBaseDataColumns()} FROM Customer";
        }

        /// <summary>
        /// Helper method to create a part of the SQL string for the columns to request the customers personal data from the database 
        /// </summary>
        /// <returns>A string of the requested columns</returns>
        public string GetCustomerBaseDataColumns()
        {
            return "FirstName, LastName, Country, PostalCode, Phone, Email";
        }

        /// <summary>
        /// Helper method to create a basic setup for reading the customers personal data from the database
        /// </summary>
        /// <param name="sql">The SQL string to use in the SQL command</param>
        /// <param name="message">The message that is displayed in the console when performing this query</param>
        /// <returns>A list of customers</returns>
        private List<Customer> PerformSqlQuery(string sql, string message)
        {
            List<Customer> selectedCustomers = new List<Customer>();

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    try
                    {
                        Console.WriteLine(message);
                        connection.Open();

                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    selectedCustomers.Add(GetCustomerFromData(reader));
                                }
                            }
                        }
                    }
                    catch (SqlException error)
                    {
                        Console.WriteLine("Sql error: " + error.Message);
                    }
                }
            } catch (Exception error)
            {
                Console.WriteLine("Sql connection error: " + error.Message);
            }
            return selectedCustomers;
        }

        /// <summary>
        /// Checks whether the given data in the reader is null or not
        /// </summary>
        /// <param name="reader">The current sql data reader</param>
        /// <param name="data">The int value of the data that needs to be checked</param>
        /// <returns></returns>
        private string CheckIfDataExists(SqlDataReader reader, int data)
        {
            if (!reader.IsDBNull(data))
            {
                return reader.GetString(data);
            }
            else
            {
                return "data does not exist";
            }
        }

        /// <summary>
        /// Helper method to read the customers personal data from the database
        /// </summary>
        /// <param name="reader">The current sql data reader</param>
        /// <returns>A new customer with the data from the reader</returns>
        private Customer GetCustomerFromData(SqlDataReader reader)
        {
            return new Customer()
            {
                ID = reader.GetInt32(0),
                FirstName = CheckIfDataExists(reader, 1),
                LastName = CheckIfDataExists(reader, 2),
                Country = CheckIfDataExists(reader, 3),
                PostalCode = CheckIfDataExists(reader, 4),
                Phone = CheckIfDataExists(reader, 5),
                Email = CheckIfDataExists(reader, 6)
            };
        }
    }
}
