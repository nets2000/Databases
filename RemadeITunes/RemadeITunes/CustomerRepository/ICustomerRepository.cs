﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using RemadeITunes.Customers;

namespace RemadeITunes.CustomerRepository
{
    public interface ICustomerRepository
    {
        // Create a string that sets parameters for connecting to database
        public void CreateConnectionString();

        // Count amount of users in database and return that number
        public int GetAmountOfCustomers();

        // Get all customers from database, return them in a list
        public List<Customer> GetAllCustomers();
        
        // Get a customer by given id, return customer
        public Customer GetCustomerByID(int id);
        
        // Get all customers with the given name pattern in their first name, returns them in a list
        public List<Customer> GetCustomersByName(string name);
        
        // Get all customers starting at the start and ending at the added amount, return them in a list
        public List<Customer> GetPageOfCustomers(int start, int amount);

        // Add a new customer to the database, return true if succesful, else return false
        public bool AddNewCustomer(Customer customer);
        
        // Update an existing customer in the database, return true if succesful, else return false
        public bool UpdateCustomer(int id, Customer customer);
        
        // Get customers per country, return in a dictionary where string is the country and int is the amount of customers in that country
        public Dictionary<string, int> GetAmountOfCustomersPerCountry();

        // Get amount of money spend per customer, return in a dictionary where string is the name of the customer and
        // float is the amount of money that customer spend
        public Dictionary<string, float> GetAmountOfMoneySpendPerCustomer();
        
        // Get favourite genres from the customer with the given id, returns CustomerGenre
        public CustomerGenre GetFavouriteGenresFromCustomer(int id);

        // Base select data from customer database query
        public string GetCustomerDataQuery();
        
        // Data columns from customer that are always selected
        public string GetCustomerBaseDataColumns();
    }
}
