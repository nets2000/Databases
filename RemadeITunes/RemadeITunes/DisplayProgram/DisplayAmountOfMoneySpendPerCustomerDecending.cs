﻿using System;
using System.Collections.Generic;
using System.Linq;
using RemadeITunes.CustomerRepository;

namespace RemadeITunes.DisplayProgram
{
    public class DisplayAmountOfMoneySpendPerCustomerDecending
    {
        /// <summary>
        /// Displays a descending list of highest spending customers
        /// </summary>
        /// <param name="customerRepository">A class that extends from the ICustomerRepository interface</param>
        public static void ShowCustomersInDecendingOrderSpendingMoney(ICustomerRepository customerRepository)
        {
            Console.WriteLine("Showing customers in decending spending order: \n------------------------------");
            Dictionary<string, float> moneySpendPerCustomer = customerRepository.GetAmountOfMoneySpendPerCustomer();

            var moneySpendPerCustomerDecending = from entry in moneySpendPerCustomer orderby entry.Value descending select entry;
            foreach (KeyValuePair<string, float> entry in moneySpendPerCustomerDecending)
            {
                Console.WriteLine($"{entry.Key} spend ${MathUtils.Round(entry.Value, 2)} on songs\n------------------------------");
            }
        }
    }
}
