﻿using System;
using System.Collections.Generic;
using RemadeITunes.Customers;
using RemadeITunes.CustomerRepository;

namespace RemadeITunes.DisplayProgram
{
    public class DisplayCustomerPage
    {
        /// <summary>
        /// Displays a page with a list of selected customers read from input of user (start index + selected amount)
        /// Will not display a page if start index is lower then 1 or higher then amount of customers
        /// Will not display a page if selected amount is lower then 1 or higher then amount of customers
        /// Will not display a page if start index + selected amount is higher then amount of customers
        /// </summary>
        /// <param name="customerRepository">A class that extends from the ICustomerRepository interface</param>
        /// <param name="amountOfCustomers">Total amount of customers in database</param>
        public static void ShowPageOfCustomers(ICustomerRepository customerRepository, int amountOfCustomers)
        {
            Console.WriteLine($"Select a customer id to start from (from 1 to {amountOfCustomers}):");
            string startInput = Console.ReadLine();

            Console.WriteLine($"Select an amount to add (from 1 to {amountOfCustomers}):");
            string amountInput = Console.ReadLine();

            int.TryParse(startInput, out int startId);
            int.TryParse(amountInput, out int amount);

            if (startId < 1 || startId > amountOfCustomers)
            {
                Console.WriteLine("Invalid start id");
            }
            else if (amount < 1 || amount > amountOfCustomers)
            {
                Console.WriteLine("Invalid added amount");
            }
            else if (startId - 1 + amount > amountOfCustomers)
            {
                Console.WriteLine("Invalid start id and amount combination");
            }
            else
            {
                List<Customer> pageOfCustomers = customerRepository.GetPageOfCustomers(startId, amount);
                ProgramDisplayer.WriteDataOfListOfCustomers(pageOfCustomers);
            }
        }
    }
}
