﻿using System;
using RemadeITunes.Customers;
using RemadeITunes.CustomerRepository;

namespace RemadeITunes.DisplayProgram
{
    public class DisplayCustomerByID
    {
        /// <summary>
        /// Displays the personal data from a single customer where the user can select the ID
        /// Will not display a customer if selected index is lower then 1 or higher then amount of customers
        /// </summary>
        /// <param name="customerRepository"></param>
        /// <param name="amountOfCustomers">Total amount of customers in database</param>
        public static void ShowCustomerByID(ICustomerRepository customerRepository, int amountOfCustomers)
        {
            Console.WriteLine($"Select an customer id to show (from 1 to {amountOfCustomers}):");
            string input = Console.ReadLine();
            int.TryParse(input, out int selectedId);
            if (selectedId > 0 && selectedId <= amountOfCustomers)
            {
                Customer selectedCustomer = customerRepository.GetCustomerByID(selectedId);
                ProgramDisplayer.WriteDataOfCustomer(selectedCustomer);
            }
            else
            {
                Console.WriteLine("Select an existing id");
            }
        }
    }
}
