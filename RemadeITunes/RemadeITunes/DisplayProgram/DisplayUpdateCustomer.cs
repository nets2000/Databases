﻿using System;
using RemadeITunes.Customers;
using RemadeITunes.CustomerRepository;

namespace RemadeITunes.DisplayProgram
{
    public class DisplayUpdateCustomer
    {
        /// <summary>
        /// Update an existing costumer input from the database by selecting an id to update.
        /// Program will ask for first name, last name, country, postal code, phone, email to fill in
        /// Will not update customer if selected index is lower then 1 or higher then amount of customers
        /// If any field is empty, no user will be addded
        /// </summary>
        /// <param name="customerRepository">A class that extends from the ICustomerRepository interface</param>
        /// <param name="amountOfCustomers">Total amount of customers in database</param>
        public static void UpdateCustomer(ICustomerRepository customerRepository, int amountOfCustomers)
        {
            Console.WriteLine("Update customer:");

            Console.WriteLine($"Which customer ID to update (from 1 to {amountOfCustomers}):");
            string input = Console.ReadLine();
            int.TryParse(input, out int selectedId);

            Customer updatedCustomer = ProgramDisplayer.CreateDataForCustomer();

            if (selectedId < 1 || selectedId > amountOfCustomers)
            {
                Console.WriteLine("Select an existing id");
            }
            else if (updatedCustomer.CheckIfFieldsAreEmpty())
            {
                Console.WriteLine("All fields need to be filled in!");
            }
            else
            {
                if (customerRepository.UpdateCustomer(selectedId, updatedCustomer))
                {
                    Console.WriteLine("Customer updated");
                }
                else
                {
                    Console.WriteLine("Failed to update customer");
                }
            }
        }
    }
}
