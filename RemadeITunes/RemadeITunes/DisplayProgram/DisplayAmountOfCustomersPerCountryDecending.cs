﻿using System;
using System.Collections.Generic;
using System.Linq;
using RemadeITunes.CustomerRepository;

namespace RemadeITunes.DisplayProgram
{
    public class DisplayAmountOfCustomersPerCountryDecending
    {
        /// <summary>
        /// Displays a descending list of the amount of customers per country
        /// </summary>
        /// <param name="customerRepository">A class that extends from the ICustomerRepository interface</param>
        public static void ShowAmountOfCustomersInEachCountryInDecendingOrder(ICustomerRepository customerRepository)
        {
            Dictionary<string, int> customersPerCountry = customerRepository.GetAmountOfCustomersPerCountry();
            Console.WriteLine("------------------------------");

            var decendingCustomersPerCountry = from entry in customersPerCountry orderby entry.Value descending select entry;
            foreach (KeyValuePair<string, int> entry in decendingCustomersPerCountry)
            {
                string message = $"{entry.Key} has {entry.Value} of customer";
                if (entry.Value > 1)
                {
                    message += "s";
                }

                Console.WriteLine(message + "\n------------------------------");
            }
        }
    }
}
