﻿using System;
using RemadeITunes.CustomerRepository;
using RemadeITunes.Customers;

namespace RemadeITunes.DisplayProgram
{
    public class DisplayFavoriteGenresOfCustomer
    {
        /// <summary>
        /// Displays the favourite genre of a customer selected by ID from user input
        /// If the customer does not have any songs it will display that instead
        /// </summary>
        /// <param name="customerRepository">A class that extends from the ICustomerRepository interface</param>
        /// <param name="amountOfCustomers">Total amount of customers in database</param>
        public static void ShowCustomerFavouriteGenres(ICustomerRepository customerRepository, int amountOfCustomers)
        {
            Console.WriteLine($"Select a customer id to show favourite genres from (from 1 to {amountOfCustomers}):");
            string input = Console.ReadLine();
            int.TryParse(input, out int selectedId);
            if (selectedId > 0 && selectedId <= amountOfCustomers)
            {
                CustomerGenre customerGenre = customerRepository.GetFavouriteGenresFromCustomer(selectedId);
                if (customerGenre.FavoriteGenres != "")
                {
                    Console.WriteLine($"Favourite genre of {customerGenre.FirstName} {customerGenre.LastName} is: " +
                        $"{customerGenre.FavoriteGenres}");
                }
                else
                {
                    Customer customer = customerRepository.GetCustomerByID(selectedId);
                    Console.WriteLine($"{customer.FirstName} {customer.LastName} does not have any songs");
                }
            }
            else
            {
                Console.WriteLine("Select an existing id");
            }
        }
    }
}
