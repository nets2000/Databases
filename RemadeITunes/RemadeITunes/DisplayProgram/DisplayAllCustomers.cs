﻿using System;
using System.Collections.Generic;
using RemadeITunes.Customers;
using RemadeITunes.CustomerRepository;

namespace RemadeITunes.DisplayProgram
{
    public class DisplayAllCustomers
    {
        /// <summary>
        /// Displays a list of personal data from every customer in the database
        /// </summary>
        /// <param name="customerRepository">A class that extends from the ICustomerRepository interface</param>
        public static void ShowAllCustomers(ICustomerRepository customerRepository)
        {
            List<Customer> allCustomers = customerRepository.GetAllCustomers();
            ProgramDisplayer.WriteDataOfListOfCustomers(allCustomers);
        }
    }
}
