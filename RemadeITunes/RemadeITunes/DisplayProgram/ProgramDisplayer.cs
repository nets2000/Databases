﻿using System;
using System.Collections.Generic;
using RemadeITunes.CustomerRepository;
using RemadeITunes.Customers;

namespace RemadeITunes.DisplayProgram
{
    public class ProgramDisplayer
    {
        /// <summary>
        /// Returns a true or false bool and performs necessary action depending on input from user
        /// True means the program keeps running
        /// False means the program wil exit
        /// </summary>
        /// <returns>Returns a bool that is true or false depending on input</returns>
        public static bool IsUsingProgram()
        {
            Console.Clear();
            Console.WriteLine("1) Read all users");
            Console.WriteLine("2) Get user by id");
            Console.WriteLine("3) Get user by name");
            Console.WriteLine("4) Return page of users");
            Console.WriteLine("5) Add user");
            Console.WriteLine("6) Update customer based on id");
            Console.WriteLine("7) Get numbers of customers in each country");
            Console.WriteLine("8) Show customers in decending order of spending money");
            Console.WriteLine("9) Show the most popular genres of customer in decending order");
            Console.WriteLine("10) Exit program");

            string input = Console.ReadLine();
            Console.Clear();
            bool stillUsingProgram = true;

            switch (input)
            {
                case "1":
                    DisplayAllCustomers.ShowAllCustomers(SQLHelper.instance);
                    break;
                case "2":
                    DisplayCustomerByID.ShowCustomerByID(SQLHelper.instance, SQLHelper.instance.AmountOfCustomers);
                    break;
                case "3":
                    DisplayCustomerByName.ShowCustomerByName(SQLHelper.instance);
                    break;
                case "4":
                    DisplayCustomerPage.ShowPageOfCustomers(SQLHelper.instance, SQLHelper.instance.AmountOfCustomers);
                    break;
                case "5":
                    DisplayInsertNewCustomer.InsertCustomer(SQLHelper.instance);
                    break;
                case "6":
                    DisplayUpdateCustomer.UpdateCustomer(SQLHelper.instance, SQLHelper.instance.AmountOfCustomers);
                    break;
                case "7":
                    DisplayAmountOfCustomersPerCountryDecending.ShowAmountOfCustomersInEachCountryInDecendingOrder(SQLHelper.instance);
                    break;
                case "8":
                    DisplayAmountOfMoneySpendPerCustomerDecending.ShowCustomersInDecendingOrderSpendingMoney(SQLHelper.instance);
                    break;
                case "9":
                    DisplayFavoriteGenresOfCustomer.ShowCustomerFavouriteGenres(SQLHelper.instance, SQLHelper.instance.AmountOfCustomers);
                    break;
                case "10":
                    stillUsingProgram = false;
                    Console.WriteLine("Program will exit on next button press");
                    break;
                default:
                    Console.WriteLine("Please select an option");
                    break;
            }

            Console.WriteLine("Press enter to continue");
            Console.ReadLine();
            return stillUsingProgram;
        }

        /// <summary>
        /// Helper method to request input data for a new customer
        /// </summary>
        /// <returns>The data of a new customer filled in by user</returns>
        public static Customer CreateDataForCustomer()
        {
            Customer newCustomer = new Customer();

            Console.WriteLine("First name: ");
            newCustomer.FirstName = Console.ReadLine();

            Console.WriteLine("Last name: ");
            newCustomer.LastName = Console.ReadLine();

            Console.WriteLine("Country: ");
            newCustomer.Country = Console.ReadLine();

            Console.WriteLine("Postal code: ");
            newCustomer.PostalCode = Console.ReadLine();

            Console.WriteLine("Phone: ");
            newCustomer.Phone = Console.ReadLine();

            Console.WriteLine("Email: ");
            newCustomer.Email = Console.ReadLine();

            return newCustomer;
        }

        /// <summary>
        /// Helper method to write down the requested data from a list of customers
        /// </summary>
        /// <param name="selectedCustomers">List of customers that needs to be written down</param>
        public static void WriteDataOfListOfCustomers(List<Customer> selectedCustomers)
        {
            for (int i = 0; i < selectedCustomers.Count; i++)
            {
                WriteDataOfCustomer(selectedCustomers[i]);
                Console.WriteLine("----------------------");
            }
        }

        /// <summary>
        /// Helper method to write down the requested data from a single customer
        /// </summary>
        /// <param name="customer">Customer that needs to be written down</param>
        public static void WriteDataOfCustomer(Customer customer)
        {
            Console.WriteLine($"user ID: {customer.ID}\n" +
                $"Name: {customer.FirstName} {customer.LastName}\n" +
                $"Country: {customer.Country}\n" +
                $"Postal code: {customer.PostalCode}\n" +
                $"Phone number: {customer.Phone}\n" +
                $"E-mail: {customer.Email}");
        }
    }
}
