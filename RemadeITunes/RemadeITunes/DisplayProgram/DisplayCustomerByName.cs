﻿using System;
using System.Collections.Generic;
using RemadeITunes.CustomerRepository;
using RemadeITunes.Customers;

namespace RemadeITunes.DisplayProgram
{
    public class DisplayCustomerByName
    {
        /// <summary>
        /// Displays data from a single customer where the user can select (part of) the name
        /// Will not display data if no data for the selected (part of) name can be found
        /// </summary>
        /// <param name="customerRepository">A class that extends from the ICustomerRepository interface</param>
        public static void ShowCustomerByName(ICustomerRepository customerRepository)
        {
            Console.WriteLine("Select a customer with first name to show:");
            string input = Console.ReadLine();

            List<Customer> selectedCustomer = customerRepository.GetCustomersByName(input);
            if (selectedCustomer.Count > 0)
            {
                ProgramDisplayer.WriteDataOfListOfCustomers(selectedCustomer);
            }
            else
            {
                Console.WriteLine("That first name does not exist");
            }
        }
    }
}
