﻿using System;
using RemadeITunes.Customers;
using RemadeITunes.CustomerRepository;

namespace RemadeITunes.DisplayProgram
{
    public class DisplayInsertNewCustomer
    {
        /// <summary>
        /// Add a newly created costumer input to the database.
        /// Program will ask for first name, last name, country, postal code, phone, email to fill in
        /// If any field is empty, no user will be addded
        /// </summary>
        /// <param name="customerRepository">A class that extends from the ICustomerRepository interface</param>
        public static void InsertCustomer(ICustomerRepository customerRepository)
        {
            Console.WriteLine("Adding new customer:");
            Customer newCustomer = ProgramDisplayer.CreateDataForCustomer();

            if (newCustomer.CheckIfFieldsAreEmpty())
            {
                Console.WriteLine("All fields need to be filled in!");
            }
            else
            {
                if (customerRepository.AddNewCustomer(newCustomer))
                {
                    Console.WriteLine("Customer added");
                    SQLHelper.instance.AmountOfCustomers = SQLHelper.instance.GetAmountOfCustomers();
                }
                else
                {
                    Console.WriteLine("Failed to add customer");
                }
            }
        }
    }
}
