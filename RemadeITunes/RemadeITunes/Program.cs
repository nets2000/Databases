﻿using RemadeITunes.CustomerRepository;
using RemadeITunes.DisplayProgram;

namespace RemadeITunes
{
    class Program
    {
        /// <summary>
        /// Create sql helper and start the program loop
        /// </summary>
        static void Main(string[] args)
        {
            SQLHelper sqlHelper = new SQLHelper();
            bool isCheckingDataBase = true;
            while (isCheckingDataBase)
            {
                isCheckingDataBase = ProgramDisplayer.IsUsingProgram();
            }
        }
    }
}
