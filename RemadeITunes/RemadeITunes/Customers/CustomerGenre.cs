﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemadeITunes.Customers
{
    /// <summary>
    /// Adds the FavoriteGenres field to the Customer
    /// </summary>
    public class CustomerGenre : Customer
    {
        public string FavoriteGenres { get; set; }
    }
}
