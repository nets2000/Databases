﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemadeITunes.Customers
{
    /// <summary>
    /// Holds all fields that a customer has in the program
    /// </summary>
    public class Customer
    {
        public int ID { get; set; } = 0;
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        /// <summary>
        /// Method that checks whether all the values of customer are filled in
        /// </summary>
        /// <returns>A bool that is true if all values containt something else returns false</returns>
        public bool CheckIfFieldsAreEmpty()
        {
            if (FirstName.Trim() == "" || LastName.Trim() == "" || Country.Trim() == "" || PostalCode.Trim() == "" ||
                Phone.Trim() == "" || Email.Trim() == "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
