﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemadeITunes
{
    public static class MathUtils
    {
        /// <summary>
        /// Helper method to create a round a number to the given amount of decimals
        /// </summary>
        /// <param name="value">Number to round</param>
        /// <param name="digits">Amount of digits to round down to</param>
        /// <returns>The value with the given amount of given digits</returns>
        public static float Round(float value, int digits)
        {
            float mult = MathF.Pow(10, digits);
            return MathF.Round(value * mult) / mult;
        }
    }
}
