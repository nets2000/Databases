USE SuperHeroesDb;

CREATE TABLE SuperHero (
	ID int PRIMARY KEY IDENTITY(1,1),
	HeroName varchar(100) null,
	Alias varchar(100) null,
	Origin varchar(100) null
)

CREATE TABLE Assistant (
	ID int PRIMARY KEY IDENTITY(1,1),
	AssistantName varchar(100) null
)

CREATE TABLE SuperPower (
	ID int PRIMARY KEY IDENTITY(1,1),
	PowerName varchar(100) null,
	PowerDescription varchar(100) null
)