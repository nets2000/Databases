USE SuperHeroesDb;

INSERT INTO SuperPower(PowerName, PowerDescription) 
VALUES('Fire ball', 'Hot')

INSERT INTO SuperPower(PowerName, PowerDescription) 
VALUES('Freeze', 'Cold')

INSERT INTO SuperPower(PowerName, PowerDescription) 
VALUES('Laser', 'Pew pew')

INSERT INTO SuperPower(PowerName, PowerDescription) 
VALUES('Lightning', 'Shocking')

INSERT INTO LinkSuperheroToPowers(HeroID, PowerID) 
VALUES(1, 1)

INSERT INTO LinkSuperheroToPowers(HeroID, PowerID) 
VALUES(1, 2)

INSERT INTO LinkSuperheroToPowers(HeroID, PowerID) 
VALUES(1, 3)

INSERT INTO LinkSuperheroToPowers(HeroID, PowerID) 
VALUES(2, 1)

INSERT INTO LinkSuperheroToPowers(HeroID, PowerID) 
VALUES(3, 4)

select * from SuperPower;
select * from LinkSuperheroToPowers;