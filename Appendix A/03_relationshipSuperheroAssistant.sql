USE SuperHeroesDb;

ALTER TABLE Assistant 
	ADD HeroID int CONSTRAINT FK_Hero_Assistant FOREIGN KEY REFERENCES SuperHero(ID)