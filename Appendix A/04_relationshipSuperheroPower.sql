USE SuperHeroesDb;

CREATE TABLE LinkSuperheroToPowers (
	HeroID int CONSTRAINT FK_Link_Hero FOREIGN KEY REFERENCES SuperHero(ID),
	PowerID int CONSTRAINT FK_Link_Power FOREIGN KEY REFERENCES SuperPower(ID),
	PRIMARY KEY (HeroID, PowerID)
)