# Databases
Querys that need to be run to create a superhero database in SQL Management server studio and a c# application that pulls information from the new iTunes platform.

# Installation & start-up
Fork to project to you're own git repository or download the zip file.<br>
Then open the project in visual studio. Then edit this file:<br>
![alt text](/RemadeITunes/RemadeITunes/ReadMeImages/DownloadProgram3.PNG?raw=true)<br>
In this file this line needs to be edited:<br>
![alt text](/RemadeITunes/RemadeITunes/ReadMeImages/DownloadProgram4.PNG?raw=true)<br>
The local server name is the same server name as SQL Management server studio connects to.
Also make sure you have the Chinook database up and running in SQL Management server studio.
Then in visual studio double click this file:<br>
![alt text](/RemadeITunes/RemadeITunes/ReadMeImages/DownloadProgram1.PNG?raw=true)<br>
Then to start the program click this button:<br>
![alt text](/RemadeITunes/RemadeITunes/ReadMeImages/DownloadProgram2.PNG?raw=true)<br>

# Usage
Fill in the input that is given by the program as viable input, else the program will ask for viable input again.

# Colleberators

Nienke Kapers: https://gitlab.com/nienkek2101
